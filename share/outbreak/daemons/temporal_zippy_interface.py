"""
This requires zippy is running.

It will take the request from ODS, check the time, and set
a style based on that time.  It will then forward the request
to zippyc.
"""
from sluice.api import Sluice, TEXTURE_RESPONSE_POOL, APP, PROTO, Protein
import os
from datetime import datetime
import bisect
from PIL import Image
from cStringIO import StringIO

STYLES = {}
TIMES = []
SVG_DIR = os.path.join(os.path.dirname(__file__), '..', 'svgs')


def _get_style(timestamp):
    global STYLES
    global TIMES
    if not STYLES:
        for fname in os.listdir(SVG_DIR):
            if fname.endswith('.svg'):
                date = fname[-14:-4]
                dt = datetime.strptime(date,'%Y-%m-%d')
                file_ts = (dt - datetime(1970, 1, 1)).total_seconds()
                STYLES[file_ts] = fname[:-4]
                TIMES.append(file_ts)
        TIMES.sort()

    #based on timestamp, find the time
    floored_index = bisect.bisect(TIMES, timestamp) - 1
    if floored_index < 0:
        #draw nothing
        return None
    else:
        return STYLES[TIMES[floored_index]]

class TemporalZippy(Sluice):
    name = 'TemporalZippy'

    texture_listen = True
    edge_listen = False
    network_listen = False

    def await(self, names=None):
        ## texture fluoros only need to await on sluice-to-fluoro
        ## and can probe on descrips
        hose = self.get_hose(TEXTURE_RESPONSE_POOL)
        return hose.await_probe_frwd([APP, PROTO, 'request', 'texture', self.name])

    def handle_texture_request(self, p):
        #We will tweak this so zippyc can serve it.
        ing = p.ingests()
        des = p.descrips()

        #Setup descrips
        des[5] = 'Zippy' #forward to the zippy fluoro

        #Setup ingests
        timestamp = ing.get('time', 0)
        new_style = _get_style(timestamp)
        print new_style
        if new_style is None:
            #Send an empty image back
            width, height = ing['px']
            img = Image.new('RGBA', (width, height))
            data = StringIO()
            img.save(data, 'PNG')
            data.seek(0)
            self.send_texture(p, data)
        else:
            ing['style'] = 'outbreak/svgs/' + new_style
            self.send_to_pool(Protein(des, ing), TEXTURE_RESPONSE_POOL)

def main():
    s = TemporalZippy()
    s.run()
    s.quit()

if '__main__' == __name__:
    main()
