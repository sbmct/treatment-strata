import csv 
import math
from random import randint

with open ('va_network.csv', 'rb') as csv_file:
    with open('output.csv', 'w') as csv_output:
        reader = csv.reader(csv_file, delimiter=',')
        writer = csv.writer(csv_output, delimiter= ',')
        for row in reader:
            if row[0] == "fac_name":
                writer.writerow(row+["Timestamp"])
            else:
                if row[4] == "VA Medical Center":
                    if row[10] == "205":
                        timestamp = 1404648000
                        admission_level = 0
                        wait_time = 0
                        budget = 0
                        patient_complaints = 0
                        for i in xrange(34):
                            admission_level = 55 + randint(-10,10)
                            wait_time = 55 + randint(-10,10)
                            budget = 55 + randint(-10,10)
                            patient_complaints = 55 + randint(-10,10)
                            writer.writerow(row+[str(timestamp)]+[str(admission_level)]+[str(wait_time)]+[str(budget)]+[str(patient_complaints)])
                            timestamp+=86400*7
                        for i in xrange(26):
                            admission_level = 55 + i*1.5+ randint(-10,10)
                            wait_time = 55 + i*1.5 + randint(-10,10)
                            budget = 55 + i*1.5 + randint(-10,10)
                            patient_complaints = 55 + i*1.5 + randint(-10,10)
                            writer.writerow(row+[str(timestamp)]+[str(admission_level)]+[str(wait_time)]+[str(budget)]+[str(patient_complaints)])
                            timestamp+=86400*7
                    else:
                        timestamp = 1404648000
                        admission_level = 0
                        wait_time = 0
                        budget = 0
                        patient_complaints = 0
                        for i in xrange(34+26):
                            admission_level = 85 + randint(-10,10)
                            wait_time = 85 + randint(-10,10)
                            budget = 85 + randint(-10,10)
                            patient_complaints = 85 + randint(-10,10)
                            writer.writerow(row+[str(timestamp)]+[str(admission_level)]+[str(wait_time)]+[str(budget)]+[str(patient_complaints)])
                            timestamp+=86400*7
